const weatherData = {
    tempUnit: 'C',
    windSpeedUnit: 'm/s',
    days:[
        {day: 'Mon', temp: 22, windDirection: 'north-east', windSpeed: 18, type:'sunny'},
        {day: 'Tue', temp: 24, windDirection: 'south-east', windSpeed: 14, type:'rainy'},
        {day: 'Wed', temp: 26, windDirection: 'north-west', windSpeed: 20, type:'cloudy'},
        {day: 'Thu', temp: 28, windDirection: 'south-west', windSpeed: 11, type:'rainy'},
        {day: 'Fri', temp: 21, windDirection: 'north-east', windSpeed: 13, type:'sunny'}
    ]
}

var activeclass = "#wed"
$(document).ready(function(){
$(activeclass).toggleClass('active');
})

$("#mon").click(function(){
    $(activeclass).toggleClass('active');
    $("#mon").toggleClass('active');
     activeclass = "#mon"
    $("#temp").html(weatherData.days[0].temp + "°");
    $("#dayval").html(weatherData.days[0].day);
    $("#windval").html(`${weatherData.days[0].windSpeed} ${weatherData.windSpeedUnit} ${weatherData.days[0].windDirection}`);
    if (weatherData.days[0].type == 'sunny')
    $("#weatherimg").attr('src','https://i.imgur.com/Shrg84B.png');
    else
     $("#weatherimg").attr('src','https://i.imgur.com/BeWfUuG.png');

})

$("#tue").click(function(){
    $(activeclass).toggleClass('active');
    $("#tue").toggleClass('active');
     activeclass = "#tue"
    $("#temp").html(weatherData.days[1].temp + "°");
    $("#dayval").html(weatherData.days[1].day);
    $("#windval").html(`${weatherData.days[1].windSpeed} ${weatherData.windSpeedUnit} ${weatherData.days[1].windDirection}`);
    if (weatherData.days[1].type == 'sunny')
    $("#weatherimg").attr('src','https://i.imgur.com/Shrg84B.png');
    else
     $("#weatherimg").attr('src','https://i.imgur.com/BeWfUuG.png');


})

$("#wed").click(function(){
    $(activeclass).toggleClass('active');
    $("#wed").toggleClass('active');
     activeclass = "#wed"
    $("#temp").html(weatherData.days[2].temp + "°");
    $("#dayval").html(weatherData.days[2].day);
    $("#windval").html(`${weatherData.days[2].windSpeed} ${weatherData.windSpeedUnit} ${weatherData.days[2].windDirection}`);
    if (weatherData.days[2].type == 'sunny')
    $("#weatherimg").attr('src','https://i.imgur.com/Shrg84B.png');
    else
     $("#weatherimg").attr('src','https://i.imgur.com/BeWfUuG.png');


})

$("#thu").click(function(){
    $(activeclass).toggleClass('active');
    $("#thu").toggleClass('active');
     activeclass = "#thu"
    $("#temp").html(weatherData.days[3].temp + "°");
    $("#dayval").html(weatherData.days[3].day);
    $("#windval").html(`${weatherData.days[3].windSpeed} ${weatherData.windSpeedUnit} ${weatherData.days[3].windDirection}`);
    if (weatherData.days[3].type == 'sunny')
    $("#weatherimg").attr('src','https://i.imgur.com/Shrg84B.png');
    else
     $("#weatherimg").attr('src','https://i.imgur.com/BeWfUuG.png');


})

$("#fri").click(function(){
    $(activeclass).toggleClass('active');
    $("#fri").toggleClass('active');
     activeclass = "#fri"
    $("#temp").html(weatherData.days[4].temp + "°");
    $("#dayval").html(weatherData.days[4].day);
    $("#windval").html(`${weatherData.days[4].windSpeed} ${weatherData.windSpeedUnit} ${weatherData.days[4].windDirection}`);
    if (weatherData.days[4].type == 'sunny')
    $("#weatherimg").attr('src','https://i.imgur.com/Shrg84B.png');
    else
     $("#weatherimg").attr('src','https://i.imgur.com/BeWfUuG.png');


})

$("#temp").click(function(){
    var tempstring = $("#temp").html();
    console.log(tempstring);
    if(tempstring[tempstring.length - 1] =='°')
    {
        tempstring = tempstring.substr(0, tempstring.length - 1);
        var newtemp = parseInt(tempstring);
        newtemp = newtemp + 273.15;
        $("#temp").html(newtemp + "K");
    }
    else{
        
        tempstring = tempstring.substr(0, tempstring.length - 1);
        var newtemp = parseInt(tempstring);
        newtemp = Math.round(newtemp - 273.15);
        $("#temp").html(newtemp + "°");
    }
    })